import React, { useState } from 'react';

const App = () => {
    const n = 3
    const shuffle = array => array.sort(() => Math.random() - 0.5)
    const getNewCards = () => {
        showCards()
        return shuffle([...new Array(n).keys(), ...new Array(n).keys()]).map(el => ({
        index: el,
        found: false,
        show: false
    }))
    }
    const showCards = () => {
        setTimeout(() => {
            const newCards = cards.map(card => {
               const newCard = { ...card }
               newCard.show = false
               return newCard
            })
            setCards(newCards)
            setBlocked(false)
        }, 5000)
    }
    const [cards, setCards] = useState(getNewCards())
    console.log('first', cards)
    const [firstSelected, setFirstSelected] = useState()
    const [blocked, setBlocked] = useState(true)
    const Card = ({ i, index, found, show }) => {
        const showCard = key => {
            console.log('before', cards)
            if (!blocked) {
                const newCards = cards.map(card => ({ ...card }))
                newCards[key].show = true
                setCards(newCards)
                console.log('after flip', newCards, firstSelected, key)
                
                if (firstSelected === undefined)
                    setFirstSelected(key)
                else {
                    setBlocked(true)
                    if (cards[firstSelected].index === cards[key].index) {
                        setTimeout(() => {
                            const newCards = cards.map(card => ({ ...card }))
                            newCards[firstSelected].found = true
                            newCards[firstSelected].show = false
                            newCards[key].found = true
                            newCards[key].show = false
                            setFirstSelected()
                            setCards(newCards)
                            setBlocked(false)
                            if (newCards.every(card => card.found)) {
                                setTimeout(() => {
                                    alert('you won!')
                                    setCards(getNewCards())
                                    showCards()
                                }, 500)
                            }
                            console.log('after found', newCards, firstSelected, key)
                        }, 5000)
                    } else {
                        setTimeout(() => {
                            const newCards = cards.map(card => ({ ...card }))
                            newCards[firstSelected].show = false
                            newCards[key].show = false
                            setFirstSelected()
                            setCards(newCards)
                            setBlocked(false)
                            console.log('after not found', newCards, firstSelected, key)
                        }, 1000);
                    }
                }
            }
        }

        let card
        if (!found) {
            if (show)
                card = <span style={{ margin: 20 }}><img width="150" height="200" src={"/" + index + ".png"} /></span>
            else
                card = <span style={{ margin: 20, padding: 100, backgroundColor: "lightblue" }} onClick={() => showCard(i)}>{index}</span>
        } else
            card = <span style={{ margin: 20, padding: 100, backgroundColor: "lightgray" }}></span>

        return card
    }

    showCards()

    return (<div width="400" >{cards.map((card, index) => <Card key={index} i={index}{...card} />)}</div>)
}

export default App;