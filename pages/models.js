import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { Row, Col, Divider } from 'antd';
import { blue } from '@ant-design/colors';

const App = () => {
    const [number, setNumber] = useState(1)
    const [second, setSecond] = useState(1)
    useEffect(() => {
        setNumber(4)
        setSecond(number + 1)
    })
    console.log(number, second)
    return (
        <div style={{width: 400, height: 900}}>
            <Row justify="space-between" align="top">
            <span style={{padding: 30,backgroundColor: blue[5]}}><Col span={4}> bro</Col></span>
            <span style={{padding: 30,backgroundColor: blue.primary}}><Col span={4}> bro</Col></span>
            <span style={{padding: 30,backgroundColor: blue.primary}}><Col span={4}> bro</Col></span>
            <span style={{padding: 30,backgroundColor: blue.primary}}><Col span={4}> bro</Col></span>
            <span style={{padding: 30,backgroundColor: blue.primary}}><Col span={4}> bro</Col></span>
            <span style={{padding: 30,backgroundColor: blue.primary}}><Col span={4}> bro</Col></span>
            </Row>
            <Row gutter={[48, 40]}>
                <Col span={4} />
                <Col span={4} />
                <Col span={4} />
                <Col span={4} />
                <Col span={4} />
                <Col span={4} />
            </Row>
        </div>)
}

export default App;