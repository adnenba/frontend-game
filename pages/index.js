import React, { useState } from 'react';

const App = () => {
    const shuffle = array => array.sort(() => Math.random() - 0.5)
    const [cards, setCards] = useState(shuffle([...new Array(10).keys(), ...new Array(10).keys()]).map(el => ({
        index: el,
        found: false,
        show: false
    })))
    console.log(cards)
    const [selected, setSelected] = useState()
    const [canSelect, setCanSelect] = useState(true)
    const Card = ({ i, index, found, show}) => {
        const showCard = key => {
            if (selected) {
                setSelected(cards[key].index)
                console.log('clicked')
                const newCards = cards.map(card => ({...card}))
                newCards[key].show = true
                setCards(newCards)
                console.log("new", newCards)

                setTimeout(() => {
                    
                }, 5000)
            }
        }
    
        let card
        if (!found) {
            if (show)
                card = <span style={{margin: 30}}><img width="150" height="200" src={"/"+ index +".jpg"} /></span>
            else
                card = <span style={{margin: 20, padding: 80, backgroundColor: "lightblue"}} onClick={() => showCard(i)}>{index}</span>
        } else
            card = <span style={{backgroundColor: "lightgray"}}></span>
        
        return card
    }

return (<div width="400" >{cards.map((card, key) => <Card key={key} i={key}{...card}/>)}</div>)
}

export default App;