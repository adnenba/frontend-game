import React, { useEffect, useState } from "react";

const App = () => {
  const n = 5;
  const shuffle = array => array.sort(() => Math.random() - 0.5);
  const getNewCards = () =>
    shuffle([...new Array(n).keys(), ...new Array(n).keys()]).map(el => ({
      index: el,
      found: false,
      show: true,
    }));
  const showCards = () => {
    setTimeout(() => {
      const newCards = cards.map(card => {
        const newCard = { ...card };
        newCard.show = false;
        return newCard;
      });
      setCards(newCards);
      setBlocked(false);
    }, 5000);
  };
  const [first, setFirst] = useState(true);
  const [cards, setCards] = useState(getNewCards());
  console.log("first", cards);
  const [firstSelected, setFirstSelected] = useState();
  const [blocked, setBlocked] = useState(true);
  const Card = ({ i, index, found, show }) => {
    const showCard = key => {
      console.log("before", cards);
      if (!blocked) {
        const newCards = cards.map(card => ({ ...card }));
        newCards[key].show = true;
        setCards(newCards);
        console.log("after flip", newCards, firstSelected, key);

        if (firstSelected === undefined) setFirstSelected(key);
        else {
          setBlocked(true);
          if (cards[firstSelected].index === cards[key].index) {
            setTimeout(() => {
              const newCards = cards.map(card => ({ ...card }));
              newCards[firstSelected].found = true;
              newCards[firstSelected].show = false;
              newCards[key].found = true;
              newCards[key].show = false;
              setFirstSelected();
              setCards(newCards);
              setBlocked(false);
              if (newCards.every(card => card.found)) {
                setTimeout(async () => {
                  setCards(getNewCards());
                  alert("you won!");
                  showCards();
                }, 500);
              }
              console.log("after found", newCards, firstSelected, key);
            }, 1000);
          } else {
            setTimeout(() => {
              const newCards = cards.map(card => ({ ...card }));
              newCards[firstSelected].show = false;
              newCards[key].show = false;
              setFirstSelected();
              setCards(newCards);
              setBlocked(false);
              console.log("after not found", newCards, firstSelected, key);
            }, 1000);
          }
        }
      }
    };

    const card = (
      <div style={{ margin: 20, height: 178, width: 170 }}>
        {show ? (
          <img width="100%" height="100%" src={"/" + index + ".png"} />
        ) : (
          <div
            onClick={() => (!found ? showCard(i) : null)}
            style={{
              width: "100%",
              height: "100%",
              backgroundColor: found ? "lightgray" : "lightblue",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <span> {index}</span>
          </div>
        )}
      </div>
    );

    return card;
  };

  if (first) {
    setFirst(false);
    showCards();
  }
  console.log("cards", cards);
  return (
    <div
      style={{
        width: "100vw",
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "space-around",
      }}
    >
      {cards.map((card, index) => (
        <Card key={index} i={index} {...card} />
      ))}
    </div>
  );
};

export default App;
